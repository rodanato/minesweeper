import React, {Component} from 'react';
import Grid               from "./components/grid";
import './App.css';

const Mines = 20;

class App extends Component {
  constructor() {
    super();

    this.state = {
      cols: 10,
      rows: 10,
      renderGrid: false,
      blocksWithMines: []
    };
  }

  componentDidMount() {
    this.setMines(Mines, this.state.rows, this.state.cols);

    this.setState({
      renderGrid: true,
      blocksRevealed: []
    });
  }

  setMines(minesLength, rows, cols) {
    let mineCounter = minesLength;
    let blocksWithMines = [];

    while (mineCounter > 0) {
      const randomNumber = Math.round(Math.random() * ((rows * cols) - 1));
      const rowPosition = Math.floor(randomNumber / rows);
      const colPosition = randomNumber % rows;

      blocksWithMines.push(rowPosition + ',' + colPosition);
      mineCounter--;
    }

    this.setState({blocksWithMines: blocksWithMines});
  }

  revealBlocks(blockList) {
    console.log(blockList, 'siblings');
    let revealedBlocks = blockList
      .filter(block =>
        !this.state.blocksWithMines.includes(block));

    this.setState({blocksRevealed: this.state.blocksRevealed.concat(revealedBlocks) });
  }

  render() {
    return (this.state.renderGrid)
           ? (
             <Grid
               rows={this.state.rows}
               cols={this.state.cols}
               blocksWithMines={this.state.blocksWithMines}
               blocksRevealed={this.state.blocksRevealed}
               onBlockClick={(blockList) => this.revealBlocks(blockList)}
             />
           )
           : null
  }
}

export default App;
