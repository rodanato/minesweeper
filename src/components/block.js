import React, {Component} from 'react';

class Block extends Component {
  position = '';

  state = {
    revealed: false,
    marked: false,
    hasAMine: false,
    minesAround: 0,
    siblings: []
  };

  componentWillMount() {
    this.position = this.props.row + ',' + this.props.col;
  }

  componentDidMount() {
    let siblings = getSiblings(this.props.row, this.props.col, this.props.rowLength, this.props.colLength);
    this.setState({ siblings: siblings });
    this.detectIfHasAMine(this.position);
    this.setMinesAround(siblings);
  }

  componentDidUpdate() {
    this.lookIfWasRevealed(this.props.blocksRevealed, this.position);
  }

  lookIfWasRevealed(revealed, position) {
    console.log(revealed.includes(position), 'is on revaled ');
    if (!this.state.marked && !this.state.revealed && revealed.includes(position)) {
      this.setState({
        revealed: true
      });

      if (this.state.minesAround === 0) {
        const siblingsAndBlock = this.state.siblings;
        siblingsAndBlock.push(this.position);

        this.props.onBlockClick(siblingsAndBlock);
      }
    }
  }

  shouldComponentUpdate() {
    return !this.state.revealed;
  }

  markBlock(e) {
    e.preventDefault();

    this.setState({
      marked: !this.state.marked
    });
  }

  setMinesAround(siblings) {
    this.setState({
      minesAround: this.getSiblingsMineNumber(siblings)
    });
  }

  detectIfHasAMine(position) {
    this.setState({
      hasAMine: hasAMine(position, this.props.blocksWithMines)
    });
  }

  getSiblingsMineNumber(siblings) {
    return siblings
      .reduce((total, sibling) =>
        this.props.blocksWithMines.includes(sibling) ? 1 + total : total, 0);
  }

  returnCssClass(state) {
    if (state.marked) {
      return 'marked';
    }
    if (state.revealed) {
      return 'revealed';
    }
    if (state.hasAMine) {
      return 'with-mine';
    }
    if (!state.hasAMine) {
      return 'without-mine';
    }
  }

  clickAction(siblingsAndBlock) {
    if (!this.state.revealed) {
      this.props.onBlockClick(siblingsAndBlock);
    }
  }

  render() {
    const siblingsAndBlock = this.state.siblings;
    siblingsAndBlock.push(this.position);

    return (
      <td
        key={this.position}
        className={this.returnCssClass(this.state)}
        onClick={() => this.clickAction(siblingsAndBlock)}
        onContextMenu={(e) => this.markBlock(e)}
      >
        {
          (this.state.minesAround !== 0 && !this.state.hasAMine)
          ? this.state.minesAround
          : null
        }
      </td>
    );
  }
}

const hasAMine = (position, blocksWithMines) => blocksWithMines.includes(position);

const getSiblings = (posY, posX, rowLength, colLength) => {
  let siblingList = [];

  if (isOnFirstRow(posY)) {
    siblingList = getBottomCenterSibling(siblingList, posY, posX);

    if (isOnFirstCol(posX)) {    //three surrounding zones
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
      siblingList = getBottomRightSibling(siblingList, posY, posX);
    } else if (isOnLastCol(posX, colLength)) {  //three surrounding zones
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
      siblingList = getBottomLeftSibling(siblingList, posY, posX);
    } else {     //five surrounding zones
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
      siblingList = getBottomLeftSibling(siblingList, posY, posX);
      siblingList = getBottomRightSibling(siblingList, posY, posX);
    }
  } else if (isOnLastRow(posY, rowLength)) {
    siblingList = getTopCenterSibling(siblingList, posY, posX);

    if (isOnFirstCol(posX)) {    //three surrounding zones
      siblingList = getTopRightSibling(siblingList, posY, posX);
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
    } else if (isOnLastCol(posX, colLength)) {  //three surrounding zones
      siblingList = getTopLeftSibling(siblingList, posY, posX);
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
    } else { //five surrounding zones
      siblingList = getTopLeftSibling(siblingList, posY, posX);
      siblingList = getTopRightSibling(siblingList, posY, posX);
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
    }
  } else {
    siblingList = getTopCenterSibling(siblingList, posY, posX);
    siblingList = getBottomCenterSibling(siblingList, posY, posX);

    if (isOnFirstCol(posX)) {                        //five surrounding zones
      siblingList = getTopRightSibling(siblingList, posY, posX);
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
      siblingList = getBottomRightSibling(siblingList, posY, posX);
    } else if (isOnLastCol(posX, colLength)) {  //five surrounding zones
      siblingList = getTopLeftSibling(siblingList, posY, posX);
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
      siblingList = getBottomLeftSibling(siblingList, posY, posX);
    } else {                                 //eight surrounding zones
      siblingList = getTopLeftSibling(siblingList, posY, posX);
      siblingList = getTopRightSibling(siblingList, posY, posX);
      siblingList = getMiddleLeftSibling(siblingList, posY, posX);
      siblingList = getMiddleRightSibling(siblingList, posY, posX);
      siblingList = getBottomLeftSibling(siblingList, posY, posX);
      siblingList = getBottomRightSibling(siblingList, posY, posX);
    }
  }
  return siblingList;
};

const isOnFirstRow = posY => isOnFirst(posY);
const isOnFirstCol = posX => isOnFirst(posX);
const isOnLastRow = (posY, rowLength) => isOnLast(posY, rowLength);
const isOnLastCol = (posX, colLength) => isOnLast(posX, colLength);

const isOnFirst = position => position === 0;
const isOnLast = (position, size) => position === (size - 1);

const getTopLeftSibling = (list, posY, posX) => list.concat((posY - 1) + ',' + (posX - 1));
const getTopCenterSibling = (list, posY, posX) => list.concat((posY - 1) + ',' + (posX));
const getTopRightSibling = (list, posY, posX) => list.concat((posY - 1) + ',' + (posX + 1));

const getBottomLeftSibling = (list, posY, posX) => list.concat((posY + 1) + ',' + (posX - 1));
const getBottomCenterSibling = (list, posY, posX) => list.concat((posY + 1) + ',' + (posX));
const getBottomRightSibling = (list, posY, posX) => list.concat((posY + 1) + ',' + (posX + 1));

const getMiddleLeftSibling = (list, posY, posX) => list.concat((posY) + ',' + (posX - 1));
const getMiddleRightSibling = (list, posY, posX) => list.concat((posY) + ',' + (posX + 1));

export default Block;
