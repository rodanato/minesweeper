import React from 'react';

import Block from "./block";

const placeCols = (props, row) => {
  let colList = [];

  for (let col = 0; col < props.cols; col++) {
    colList.push(
      <Block
        key={row + ',' + col}
        row={row}
        col={col}
        colLength={props.cols}
        rowLength={props.rows}
        blocksWithMines={props.blocksWithMines}
        blocksRevealed={props.blocksRevealed}
        onBlockClick={props.onBlockClick}
      >
      </Block>
    );
  }

  return colList;
};

const placeRows = props => {
  let rowList = [];

  for (let row = 0; row < props.rows; row++) {
    rowList.push(
      <tr key={row}>
        {placeCols(props, row)}
      </tr>
    );
  }

  return rowList;
};

const Grid = props => (
  <table className={'grid'}
         border="1">
    <tbody>
    {placeRows(props)}
    </tbody>
  </table>
);

export default Grid;